From 28de74c9944bdbdf18ffccc91d2d96a0cd4ade48 Mon Sep 17 00:00:00 2001
From: Ben Boeckel <ben.boeckel@kitware.com>
Date: Thu, 16 Dec 2021 17:08:10 -0500
Subject: [PATCH 1/3] armclang: clarify that this is support for the Keil
 cross-compiler

---
 mesonbuild/compilers/c.py          | 4 ++++
 mesonbuild/compilers/cpp.py        | 4 ++++
 mesonbuild/compilers/mixins/arm.py | 3 +++
 3 files changed, 11 insertions(+)

diff --git a/mesonbuild/compilers/c.py b/mesonbuild/compilers/c.py
index 9d4a7793451..c5314470c88 100644
--- a/mesonbuild/compilers/c.py
+++ b/mesonbuild/compilers/c.py
@@ -211,6 +211,10 @@ def __init__(self, exelist: T.List[str], version: str, for_machine: MachineChoic
 
 
 class ArmclangCCompiler(ArmclangCompiler, CCompiler):
+    '''
+    Keil armclang
+    '''
+
     def __init__(self, exelist: T.List[str], version: str, for_machine: MachineChoice, is_cross: bool,
                  info: 'MachineInfo', exe_wrapper: T.Optional['ExternalProgram'] = None,
                  linker: T.Optional['DynamicLinker'] = None,
diff --git a/mesonbuild/compilers/cpp.py b/mesonbuild/compilers/cpp.py
index 8bc013f2235..635cc55933b 100644
--- a/mesonbuild/compilers/cpp.py
+++ b/mesonbuild/compilers/cpp.py
@@ -296,6 +296,10 @@ def get_option_compile_args(self, options: 'KeyedOptionDictType') -> T.List[str]
 
 
 class ArmclangCPPCompiler(ArmclangCompiler, CPPCompiler):
+    '''
+    Keil armclang
+    '''
+
     def __init__(self, exelist: T.List[str], version: str, for_machine: MachineChoice, is_cross: bool,
                  info: 'MachineInfo', exe_wrapper: T.Optional['ExternalProgram'] = None,
                  linker: T.Optional['DynamicLinker'] = None,
diff --git a/mesonbuild/compilers/mixins/arm.py b/mesonbuild/compilers/mixins/arm.py
index 4e1898ae747..fc39851bab2 100644
--- a/mesonbuild/compilers/mixins/arm.py
+++ b/mesonbuild/compilers/mixins/arm.py
@@ -136,6 +136,9 @@ def compute_parameters_with_absolute_paths(self, parameter_list: T.List[str], bu
 
 
 class ArmclangCompiler(Compiler):
+    '''
+    This is the Keil armclang.
+    '''
 
     def __init__(self) -> None:
         if not self.is_cross:

From 786d4379824a370b3a6ea415dc48aadb3b1f9dd2 Mon Sep 17 00:00:00 2001
From: Ben Boeckel <ben.boeckel@kitware.com>
Date: Thu, 16 Dec 2021 17:08:26 -0500
Subject: [PATCH 2/3] armclang: extend the prefix detection for Keil armclang

This will avoid false positives with the ARM Ltd. `armclang` compiler.
---
 mesonbuild/build.py | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/mesonbuild/build.py b/mesonbuild/build.py
index 6515f429d80..9fc31c6b21b 100644
--- a/mesonbuild/build.py
+++ b/mesonbuild/build.py
@@ -1777,8 +1777,8 @@ def __init__(self, name: str, subdir: str, subproject: str, for_machine: Machine
                 self.suffix = 'exe'
             elif machine.system.startswith('wasm') or machine.system == 'emscripten':
                 self.suffix = 'js'
-            elif ('c' in self.compilers and self.compilers['c'].get_id().startswith('arm') or
-                  'cpp' in self.compilers and self.compilers['cpp'].get_id().startswith('arm')):
+            elif ('c' in self.compilers and self.compilers['c'].get_id().startswith('armclang') or
+                  'cpp' in self.compilers and self.compilers['cpp'].get_id().startswith('armclang')):
                 self.suffix = 'axf'
             elif ('c' in self.compilers and self.compilers['c'].get_id().startswith('ccrx') or
                   'cpp' in self.compilers and self.compilers['cpp'].get_id().startswith('ccrx')):

From cc6e2284098121b6c7511729d877d80d7a6c2b12 Mon Sep 17 00:00:00 2001
From: Ben Boeckel <ben.boeckel@kitware.com>
Date: Thu, 16 Dec 2021 17:09:06 -0500
Subject: [PATCH 3/3] armltdclang: add support for ARM Ltd.'s `armclang`
 toolchain

This is another toolchain also called `armclang`, but it is not a cross
compiler like Keil's `armclang`. It is essentially the same as `clang`
based on its interface and CMake's support of the toolchain.

Use an `armltd` prefix for the compiler ID.

Fixes: #7255
---
 mesonbuild/compilers/c.py       |  7 +++++++
 mesonbuild/compilers/cpp.py     |  8 +++++++-
 mesonbuild/compilers/detect.py  | 28 ++++++++++++++++++++++++++++
 mesonbuild/compilers/fortran.py |  6 ++++++
 4 files changed, 48 insertions(+), 1 deletion(-)

diff --git a/mesonbuild/compilers/c.py b/mesonbuild/compilers/c.py
index c5314470c88..30768ad6d9b 100644
--- a/mesonbuild/compilers/c.py
+++ b/mesonbuild/compilers/c.py
@@ -183,6 +183,13 @@ def get_option_link_args(self, options: 'KeyedOptionDictType') -> T.List[str]:
         return []
 
 
+class ArmLtdClangCCompiler(ClangCCompiler):
+
+    def __init__(self, *args, **kwargs):
+        ClangCCompiler.__init__(self, *args, **kwargs)
+        self.id = 'armltdclang'
+
+
 class AppleClangCCompiler(ClangCCompiler):
 
     """Handle the differences between Apple Clang and Vanilla Clang.
diff --git a/mesonbuild/compilers/cpp.py b/mesonbuild/compilers/cpp.py
index 635cc55933b..20df35969bf 100644
--- a/mesonbuild/compilers/cpp.py
+++ b/mesonbuild/compilers/cpp.py
@@ -155,7 +155,7 @@ def _find_best_cpp_std(self, cpp_std: str) -> str:
         }
 
         # Currently, remapping is only supported for Clang, Elbrus and GCC
-        assert self.id in frozenset(['clang', 'lcc', 'gcc', 'emscripten'])
+        assert self.id in frozenset(['clang', 'lcc', 'gcc', 'emscripten', 'armltdclang'])
 
         if cpp_std not in CPP_FALLBACKS:
             # 'c++03' and 'c++98' don't have fallback types
@@ -259,6 +259,12 @@ def language_stdlib_only_link_flags(self, env: 'Environment') -> T.List[str]:
         return search_dirs + ['-lstdc++']
 
 
+class ArmLtdClangCPPCompiler(ClangCPPCompiler):
+    def __init__(self, *args, **kwargs):
+        ClangCPPCompiler.__init__(self, *args, **kwargs)
+        self.id = 'armltdclang'
+
+
 class AppleClangCPPCompiler(ClangCPPCompiler):
     def language_stdlib_only_link_flags(self, env: 'Environment') -> T.List[str]:
         # We need to apply the search prefix here, as these link arguments may
diff --git a/mesonbuild/compilers/detect.py b/mesonbuild/compilers/detect.py
index 03c5226d2d4..572ec351a80 100644
--- a/mesonbuild/compilers/detect.py
+++ b/mesonbuild/compilers/detect.py
@@ -54,6 +54,7 @@
     AppleClangCCompiler,
     ArmCCompiler,
     ArmclangCCompiler,
+    ArmLtdClangCCompiler,
     ClangCCompiler,
     ClangClCCompiler,
     GnuCCompiler,
@@ -74,6 +75,7 @@
     AppleClangCPPCompiler,
     ArmCPPCompiler,
     ArmclangCPPCompiler,
+    ArmLtdClangCPPCompiler,
     ClangCPPCompiler,
     ClangClCPPCompiler,
     GnuCPPCompiler,
@@ -97,6 +99,7 @@
 from .cuda import CudaCompiler
 from .fortran import (
     FortranCompiler,
+    ArmLtdFlangFortranCompiler,
     G95FortranCompiler,
     GnuFortranCompiler,
     ElbrusFortranCompiler,
@@ -462,6 +465,20 @@ def sanitize(p: str) -> str:
                 ccache + compiler, version, for_machine, is_cross, info,
                 exe_wrap, linker=linker, full_version=full_version)
 
+        if 'Arm C/C++/Fortran Compiler' in out:
+            arm_ver_match = re.search('version (\d+)\.(\d+) \(build number (\d+)\)', out)
+            arm_ver_major = arm_ver_match.group(1)
+            arm_ver_minor = arm_ver_match.group(2)
+            arm_ver_build = arm_ver_match.group(3)
+            version = '.'.join([arm_ver_major, arm_ver_minor, arm_ver_build])
+            if lang == 'c':
+                cls = ArmLtdClangCCompiler
+            elif lang == 'cpp':
+                cls = ArmLtdClangCPPCompiler
+            linker = guess_nix_linker(env, compiler, cls, for_machine)
+            return cls(
+                ccache + compiler, version, for_machine, is_cross, info,
+                exe_wrap, linker=linker)
         if 'armclang' in out:
             # The compiler version is not present in the first line of output,
             # instead it is present in second line, startswith 'Component:'.
@@ -711,6 +728,17 @@ def detect_fortran_compiler(env: 'Environment', for_machine: MachineChoice) -> C
                         compiler, version, for_machine, is_cross, info,
                         exe_wrap, defines, full_version=full_version, linker=linker)
 
+            if 'Arm C/C++/Fortran Compiler' in out:
+                cls = ArmLtdFlangFortranCompiler
+                arm_ver_match = re.search('version (\d+)\.(\d+) \(build number (\d+)\)', out)
+                arm_ver_major = arm_ver_match.group(1)
+                arm_ver_minor = arm_ver_match.group(2)
+                arm_ver_build = arm_ver_match.group(3)
+                version = '.'.join([arm_ver_major, arm_ver_minor, arm_ver_build])
+                linker = guess_nix_linker(env, compiler, cls, for_machine)
+                return cls(
+                    ccache + compiler, version, for_machine, is_cross, info,
+                    exe_wrap, linker=linker)
             if 'G95' in out:
                 cls = G95FortranCompiler
                 linker = guess_nix_linker(env, compiler, cls, for_machine)
diff --git a/mesonbuild/compilers/fortran.py b/mesonbuild/compilers/fortran.py
index 6a4a343805b..e701ed8d5c8 100644
--- a/mesonbuild/compilers/fortran.py
+++ b/mesonbuild/compilers/fortran.py
@@ -494,6 +494,12 @@ def language_stdlib_only_link_flags(self, env: 'Environment') -> T.List[str]:
                 search_dirs.append(f'-L{d}')
         return search_dirs + ['-lflang', '-lpgmath']
 
+class ArmLtdFlangFortranCompiler(FlangFortranCompiler):
+
+    def __init__(self, *args, **kwargs):
+        FlangFortranCompiler.__init__(self, *args, **kwargs)
+        self.id = 'armltdflang'
+
 class Open64FortranCompiler(FortranCompiler):
 
     def __init__(self, exelist: T.List[str], version: str, for_machine: MachineChoice, is_cross: bool,
